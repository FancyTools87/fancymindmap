var express = require('express'),
    route = require("./utils/routes.js");
var app = express();

app.use(express.static(__dirname + '/public'));
app.listen(1789)